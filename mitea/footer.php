<?php
/**
 *
 * @package WordPress
 * @subpackage Mitea
 * @since 1.0
 * @version 1.0
 */
?>
    </main>
    <footer class="footer">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-lg-4">
            <div class="footer-info">
              <?php 
              $footer = get_field('footer', 'option');
              if( $footer['logo'] ) { ?>
              <div class="footer-logo">
                <img src="<?php echo $footer['logo']['url']; ?>"
                     alt="<?php echo $footer['logo']['title']; ?>">
              </div>
              <?php } ?>
              <address class="footer-address">
                <?php if( $footer['company_name'] ) { ?>
                  <span class="company-name"><?php echo $footer['company_name']; ?></span><br/>
                <?php } 
                if( $footer['company_address'] ) { ?>
                <div class="company-address">
                  <?php echo $footer['company_address']; ?>
                </div>
                <?php } ?>
                <div class="contacts">
                  <span class="contact">
                    <?php echo $footer['phone_title']; ?> 
                    <a href="tel:<?php echo $footer['phone']; ?>" class="contact-link phone"><?php echo $footer['phone']; ?></a>
                  </span>
                  <span class="contact">
                    <?php echo $footer['email_title']; ?> 
                    <a href="tel:<?php echo $footer['email']; ?>" class="contact-link email"><?php echo $footer['email']; ?></a>
                  </span>
                </div>
              </address>
            </div>
          </div>
          <div class="col-md-6 col-lg-8">
            <div class="row">
              <?php 
              if ( is_active_sidebar( 'footer-1' ) ) { ?>
                <div class="col-4">
                <?php dynamic_sidebar( 'footer-1' ); ?>
                </div>
              <?php } 
              if ( is_active_sidebar( 'footer-2' ) ) { ?>
                <div class="col-4">
                <?php dynamic_sidebar( 'footer-2' ); ?>
                </div>
              <?php } 
              if ( is_active_sidebar( 'footer-3' ) ) { ?>
                <div class="col-4">
                <?php dynamic_sidebar( 'footer-3' ); ?>
                </div>
              <?php } ?>
            </div>
          </div>
        </div>
        <div class="row mt-3 align-items-center justify-content-between">
          <div class="col-md-6">
            <div class="footer-social">
              <?php if( $footer['social_title'] ) { ?>
                <p class="footer-social-title"><?php echo $footer['social_title']; ?></p>
              <?php } ?>
              <div class="footer-social-icons">
                <?php if( $footer['facebook'] ) { ?>
                <a href="<?php echo $footer['facebook']; ?>" class="footer-social-link">
                  <img src="<?php echo get_theme_file_uri('/assets/images/icon-facebook.png'); ?>" alt="facebook">
                </a>
                <?php } 
                if( $footer['instagram'] ) { ?>
                <a href="<?php echo $footer['instagram']; ?>" class="footer-social-link">
                  <img src="<?php echo get_theme_file_uri('/assets/images/icon-instagram.png'); ?>" alt="instagram">
                </a>
                <?php } 
                if( $footer['youtube'] ) { ?>
                <a href="<?php echo $footer['youtube']; ?>" class="footer-social-link">
                  <img src="<?php echo get_theme_file_uri('/assets/images/icon-youtube.png'); ?>" alt="youtube">
                </a>
                <?php } ?>
              </div>
            </div>
          </div>
          <?php if( has_nav_menu('footer') ) { ?>
          <div class="col-md-6">
            <?php wp_nav_menu( array(
                'theme_location'        => 'footer',
                'container'             => 'nav',
                'container_class'       => 'footer-meta'
            ) ); ?>
          </div>
          <?php } ?>
        </div>
      </div>
    </footer>
    <!-- MODAL LOCATION -->
    <?php 
    $location_modal = get_field('location_modal', 'option');
    ?>
    <div class="modal-window location" data-modal="map">
      <div class="modal-form location">
        <span data-close="close"></span>
        <div class="form-headline">
          <?php if( $location_modal['title'] ) { ?>
          <p class="headline-title"><?php echo $location_modal['title']; ?></p>
          <?php } ?>
        </div>
        <div class="modal-container">
          <div class="modal-text">
            <p class="choose-location"><?php echo $location_modal['text']; ?></p>
            <?php 
            $locations = $location_modal['locations'];
            $i = 1;
            $size = sizeof($locations);

            foreach ( $locations as $location ) { ?>
              <div class="location"><?php echo $location['location']; if( $i != $size ) echo', ';?></div>
            <?php $i++; } ?>
          </div>
          <?php if( $location_modal['image'] ) { ?>
          <div class="modal-map">
            <img src="<?php echo $location_modal['image']['url']; ?>" alt="<?php echo $location_modal['image']['title']; ?>">
          </div>
          <?php } ?>
        </div>
      </div>
    </div>
    <!-- MODAL LOGIN -->
    <div class="modal-window login" data-modal="login">
      <div class="modal-form">
        <span data-close="close"></span>
        <p class="form-title">Anmelden.</p>
        <?php 
        $args = array(
            'echo' => false,
        );
        $form = wp_login_form( $args ); 
        $form = str_replace('name="log"', 'name="log" placeholder="'.__('Username or Email Address', 'mitea').'"', $form);
        $form = str_replace('name="pwd"', 'name="pwd" placeholder="'.__('Password', 'mitea').'"', $form); 
        echo $form; ?>
      </div>
    </div>
    <?php wp_footer(); ?>
</body>
</html>