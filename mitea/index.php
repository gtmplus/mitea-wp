<?php
/**
 *
 * @package WordPress
 * @subpackage Mitea
 * @since 1.0
 * @version 1.0
 */

get_header(); 
get_template_part( 'template-parts/page/content', 'banner' );
get_template_part( 'template-parts/page/content', 'breadcrumbs' );

$blog = get_field('blog', 'option');
$page = get_query_var('paged') ? get_query_var('paged') : 1;
$args = array(
	'post_type'			=> 'post',
	'posts_per_page'	=> get_option('posts_per_page'),
	'paged'				=> $page
);
$query = new WP_Query( $args ); ?>

<div class="container">
	<div class="headline-content">
      <?php if( $blog['title'] ) { ?><h2 class="headline-title"><?php echo $blog['title']; ?></h2><?php } 
      if( $blog['text'] ) { ?>
  	  <div class="headline-description">
        <?php echo $blog['text']; ?>
      </div>
      <?php } ?>
    </div>
</div>

<?php if ( $query->have_posts() ) : ?>

<div class="container">
	<section class="category-list">
      <div class="row">
      	<?php while ( $query->have_posts() ) : $query->the_post(); 
      		get_template_part( 'template-parts/post/content', 'thumbnail' );
      	endwhile; ?>
      </div>
    </section>
</div>

<?php wp_reset_postdata();
endif; 
get_template_part( 'template-parts/post/content', 'pagination' );

get_footer();