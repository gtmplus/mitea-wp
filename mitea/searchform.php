<?php
/**
 *
 * @package WordPress
 * @subpackage Mitea
 * @since 1.0
 * @version 1.0
 */
?>

<form role="search" method="get" class="mta-search__form search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<div class="mta-search__group">
		<input type="search" class="mta-search__field searchbar" value="<?php echo get_search_query(); ?>" name="s" placeholder="<?php _e('Was suchen Sie?', 'mitea'); ?>" />
		<button type="submit" class="mta-search__submit search-icon"></button>
	</div>
</form>