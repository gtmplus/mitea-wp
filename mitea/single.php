<?php
/**
 *
 * @package WordPress
 * @subpackage Mitea
 * @since 1.0
 * @version 1.0
 */

get_header(); 
get_template_part( 'template-parts/page/content', 'banner' );
get_template_part( 'template-parts/page/content', 'breadcrumbs' ); ?>

<div class="container">
	<?php if ( have_posts() ) { 
		while ( have_posts() ) { the_post(); ?>
			<article class="post">
	          <div class="headline-content">
	            <h2 class="headline-title"><?php the_title(); ?></h2>
	          </div>
	          <div class="post-content">
	            <?php the_content(); ?>
	          </div>
	        </article>
		<?php }
	} ?>
</div>

<?php 
get_template_part( 'template-parts/post/content', 'related' );
get_footer();