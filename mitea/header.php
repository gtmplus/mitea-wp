<?php
/**
 *
 * @package WordPress
 * @subpackage Mitea
 * @since 1.0
 * @version 1.0
 */
?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=1280px, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <?php wp_head();  ?>
</head>
<body <?php body_class('page-wrapper'); ?>>
    <?php wp_body_open(); ?>
    <header class="header">
      <div class="container">
        <nav class="navbar navbar-expand-lg align-items-center navbar-light">
          <?php 
          $header = get_field('header', 'option');
          if( $header['logo'] ) { ?>
          <a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>">
            <img class="header-logo" src="<?php echo $header['logo']['url']; ?>"
                 alt="<?php echo $header['logo']['title']; ?>">
          </a>
          <?php } ?>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggler" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <?php
          $cart_page = get_field('ecommerce', 'option')['cart_page'];
          ?>
          <div class="collapse navbar-collapse flex-lg-column align-items-lg-end" id="navbarToggler">
            <div class="top-nav">
              <ul>
                <li class="nav-item" data-modal-trigger="map">
                  <a class="nav-link" href="#"><img src="<?php echo get_theme_file_uri('/assets/images/mark.svg'); ?>" class="icon-mark" />Mülheim an der Ruhr</a>
                </li>
                <?php if( $cart_page ) { ?>
                <li class="nav-item">
                  <a class="nav-link" href="<?php echo get_permalink( $cart_page ); ?>">
                    <img src="<?php echo get_theme_file_uri('/assets/images/shopping-cart.svg'); ?>" class="icon-cart" />
                    <span class="counter">0</span> <?php echo get_the_title( $cart_page ); ?></a>
                </li>
                <?php } ?>
                <li class="nav-item" data-modal-trigger="login">
                  <a class="nav-link" href="#"><img src="<?php echo get_theme_file_uri('/assets/images/user.svg'); ?>" class="icon-user" />Login</a>
                </li>
              </ul>
            </div>
            <?php if( has_nav_menu('main') ) {
              wp_nav_menu( array(
                  'theme_location'        => 'main',
                  'container'             => 'nav',
                  'container_class'       => 'navbar-nav mt-2 mt-lg-0'
              ) ); 
            } ?>
          </div>
        </nav>
      </div>
    </header>
    <main>