<?php
/**
 *
 * @package WordPress
 * @subpackage Mitea
 * @since 1.0
 * @version 1.0
 */

get_header(); 
get_template_part( 'template-parts/page/content', 'banner' );
get_template_part( 'template-parts/page/content', 'breadcrumbs' ); 
$ecommerce = new MiteaEcommerceClass();
$product = $ecommerce->get_product_details();
?>
<div class="container">
	<?php if ( have_posts() ) { 
		while ( have_posts() ) { the_post(); ?>
			<section class="product-container">
	          <a href="#" class="link-back">
	            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 492 492">
	              <defs/>
	              <path d="M464.344 207.418l.768.168H135.888l103.496-103.724c5.068-5.064 7.848-11.924 7.848-19.124 0-7.2-2.78-14.012-7.848-19.088L223.28 49.538c-5.064-5.064-11.812-7.864-19.008-7.864-7.2 0-13.952 2.78-19.016 7.844L7.844 226.914C2.76 231.998-.02 238.77 0 245.974c-.02 7.244 2.76 14.02 7.844 19.096l177.412 177.412c5.064 5.06 11.812 7.844 19.016 7.844 7.196 0 13.944-2.788 19.008-7.844l16.104-16.112c5.068-5.056 7.848-11.808 7.848-19.008 0-7.196-2.78-13.592-7.848-18.652L134.72 284.406h329.992c14.828 0 27.288-12.78 27.288-27.6v-22.788c0-14.82-12.828-26.6-27.656-26.6z"/>
	            </svg>
	            <?php _e('Zurück zur Übersicht', 'mitea'); ?>
	          </a>
	          <div class="row">
	            <div class="col-lg-5">
	              <!-- Swiper -->
	              <?php if( $product['thumbnails'] ) { ?>
	              <div class="gallery-top">
	                  <?php
	                  $i = 1;
	                  $size = sizeof($product['thumbnails']);
	                  foreach ( $product['thumbnails'] as $thumbnail ) { 
	                  	$class = $i == 1 ? ' large': ''; ?>
	                  	<a class="thumbnail<?php echo $class; ?>" data-lightbox="product-image" href="<?php echo $thumbnail; ?>" style="background-image:url(<?php echo $thumbnail; ?>)"></a>
	                  	<?php if( $size > 1 && $i == 1 ) { ?>
              				<div class="small__galery">
              			<?php } elseif( $size == $i ){ ?>
              				</div>
	                  	<?php } 
	                  $i++; } ?>
                  </div>
	          	  <?php } ?>
	            </div>
	            <div class="col-lg-7">
	              <div class="product-content">
	                <!-- HEADING -->
	                <div class="product-content-heading">
	                  <h1 class="product-title"><?php echo $product['title']; ?></h1>
	                  <p class="product-articule"><?php _e('Artikel-Nr:', 'mitea'); ?>
	                    <span class="product-articule-code"><?php echo $product['article']; ?></span>
	                  </p>
	                  <p class="product-package"><?php _e('Verpackungseinheit:', 'mitea'); ?>
	                    <span class="product-package-size"><?php echo $product['packaging-unit']; ?> Stück</span>
	                  </p>
	                </div>
	                <!-- DESCRIPTION -->
	                <div class="product-description">
	                  <?php echo $product['description']; ?>
	                </div>
	                <!-- PRICING -->
	                <div class="product-price">
	                  <p class="product-price-title"><?php _e('Preise:', 'mitea'); ?></p>
	                  <div class="product-price-netto"><span class="price"><?php echo $product['price']; ?> €</span><?php _e(' zzgl. MwSt.', 'mitea'); ?></div>
	                  <div class="product-price-brutto"><span class="price"><?php echo $product['price-vat']; ?> €</span><?php _e(' inkl. MwSt.', 'mitea'); ?></div>
	                </div>

	                <!-- CART FORM -->
	                <form class="cart-form" action="" method="post">
	                  <p class="form-title"><?php _e('Stückzahl:', 'mitea'); ?></p>

	                  <div class="products-quantity">
	                    <input type="number" class="products-quantity-input" name="products-quantity" value="222">
	                  </div>

	                  <button type="submit" name="add-to-cart" value="" class="add-product btn">
	                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -31 512 512"><path d="M164.96 300.004h.024c.02 0 .04-.004.059-.004H437a15.003 15.003 0 0014.422-10.879l60-210a15.003 15.003 0 00-2.445-13.152A15.006 15.006 0 00497 60H130.367l-10.722-48.254A15.003 15.003 0 00105 0H15C6.715 0 0 6.715 0 15s6.715 15 15 15h77.969c1.898 8.55 51.312 230.918 54.156 243.71C131.184 280.64 120 296.536 120 315c0 24.812 20.188 45 45 45h272c8.285 0 15-6.715 15-15s-6.715-15-15-15H165c-8.27 0-15-6.73-15-15 0-8.258 6.707-14.977 14.96-14.996zM477.114 90l-51.43 180H177.032l-40-180zM150 405c0 24.813 20.188 45 45 45s45-20.188 45-45-20.188-45-45-45-45 20.188-45 45zm45-15c8.27 0 15 6.73 15 15s-6.73 15-15 15-15-6.73-15-15 6.73-15 15-15zm167 15c0 24.813 20.188 45 45 45s45-20.188 45-45-20.188-45-45-45-45 20.188-45 45zm45-15c8.27 0 15 6.73 15 15s-6.73 15-15 15-15-6.73-15-15 6.73-15 15-15zm0 0"/></svg>
	                    <?php _e('In den Warenkorb', 'mitea'); ?>
	                  </button>
	                </form>

	                <p class="products-info">
	                  <?php echo $product['details']; ?>
	                </p>
	              </div>
	            </div>
	          </div>
	        </section>
		<?php }
	} ?>
</div>

<?php 
get_template_part( 'template-parts/post/content', 'related' );
get_footer();