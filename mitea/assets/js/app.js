//= ../../node_modules/jquery/dist/jquery.slim.js
//= ../../node_modules/bootstrap/dist/js/bootstrap.min.js
//= ../../node_modules/swiper/swiper-bundle.js

window.addEventListener('load', () => {

  // safe init func for sliders
  function safeInit(selector, conf) {
    if (selector) {
      new Swiper(selector, conf);
    }else{
      console.log('Undefined selector, or wrong config')
    };
  };

  // modals
  const modalTriggers = document.querySelectorAll('[data-modal-trigger]');
  const modals = document.querySelectorAll('[data-modal]');
  const modalClose = document.querySelectorAll('[data-close]');
  const body = document.querySelector('body.page-wrapper');
  let currentModal;

  modalTriggers.forEach(element => {
    element.addEventListener('click', (e) => {
      e.preventDefault();
      modals.forEach(elem => {
        if (element.dataset.modalTrigger === elem.dataset.modal) {
          elem.classList.add('show');
          body.classList.add('no-scroll');
          currentModal = elem;
        }
      });
    });
  });

  modalClose.forEach(close => {
    close.addEventListener('click', () => {
      close.closest('.modal-window').classList.remove('show');
      body.classList.remove('no-scroll');
    });
  });

  modals.forEach(modal => {
    modal.addEventListener('click', (event) => {
      if( event.target.classList.contains('modal-window') ){
        modal.classList.remove('show');
        body.classList.remove('no-scroll');
      }
    });
  });

  // intro slider
  const introSlider = document.querySelector('.intro-slider');
  if(introSlider){
    const introSliderConf = {
      centeredSlides: true,
      slidesPerView: 1,
      loop: true,
      observer: true,
      observeParents: true,
      observeSlideChildren: true,
      autoplay: {
        delay: 2500,
        disableOnInteraction: false,
      },
      pagination: {
        el: '.swiper-dots',
        clickable: true,
      },
      navigation: {
        nextEl: introSlider.querySelector('.swiper-button-next'),
        prevEl: introSlider.querySelector('.swiper-button-prev'),
      },
    };

    safeInit(introSlider, introSliderConf);
  }

  // features section tab-sliders init
  const featuresSliders = document.querySelectorAll('.tab-slider');
  const navPill = document.querySelectorAll('.nav.nav-pills .nav-link');
  const tabsPane = document.querySelectorAll('.tab-content .tab-pane');

  const sliders = [
    '.tab-slider-gartenparty',
    '.tab-slider-firmenfeier',
    '.tab-slider-hochzeit',
    '.tab-slider-messe',
    '.tab-slider-weihnachtsfeier'
  ];
  let i = j = 0;
  // console.log(featuresSliders);
  var initSlider = [];
  featuresSliders.forEach(slider => {
    let featuresSlidersConf = {
      centeredSlides: false,
      slidesPerView: 3,
      observer: true,
      observeParents: true,
      observeSlideChildren: true,
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
      navigation: {
        nextEl: slider.closest('.tab-pane').querySelector('.swiper-button-next'),
        prevEl: slider.closest('.tab-pane').querySelector('.swiper-button-prev'),
      },
      // Responsive breakpoints
      breakpoints: {
        // when window width is >= 320px
        320: {
          slidesPerView: 1,
          spaceBetween: 10
        },
        // when window width is >= 480px
        480: {
          slidesPerView: 1,
          spaceBetween: 10
        },
        // when window width is >= 640px
        640: {
          slidesPerView: 1,
          spaceBetween: 10
        },
        // when window width is >= 768px
        768: {
          slidesPerView: 2,
          spaceBetween: 20
        },
        // when window width is >= 992px
        992: {
          slidesPerView: 3,
          spaceBetween: 20
        }
      }
    };
    
    initSlider[i] = new Swiper(slider, featuresSlidersConf);
    i++;
  });
  
  navPill.forEach((pill) => {
    pill.addEventListener('click', () => {
      let tag   = pill.dataset.tag,
          index = Array.prototype.indexOf.call(navPill, pill);

      navPill.forEach( (item) => {
        item.classList.remove('active');
      })
      tabsPane.forEach( (tab) => {
        if( tab.dataset.tag == tag ){
          tab.classList.add('active');
          tab.classList.add('show');
        } else {
          tab.classList.remove('active');
          tab.classList.remove('show');
        }
      })
      pill.classList.add('active');
      initSlider[index].slideTo(0);
    });
    j++;
  })


  // section search-form
  const keywords = document.querySelectorAll('.search-keyword');
  const search = document.querySelector('.searchbar');

  keywords.forEach((element) => {
    element.addEventListener("click", () => {
      search.value = element.textContent;
    });
  });

  // inspiration section slider
  const inspSlider = document.querySelector('.inspiration-slider');
  if( inspSlider ){
    const inspSliderConf = {
      centeredSlides: false,
      slidesPerView: 3,
      loop: true,
      navigation: {
        nextEl: inspSlider.closest('.slider-wrapper').querySelector('.swiper-button-next'),
        prevEl: inspSlider.closest('.slider-wrapper').querySelector('.swiper-button-prev'),
      },
      // Responsive breakpoints
      breakpoints: {
        // when window width is >= 320px
        320: {
          slidesPerView: 1,
          spaceBetween: 10
        },
        // when window width is >= 480px
        480: {
          slidesPerView: 1,
          spaceBetween: 10
        },
        // when window width is >= 640px
        640: {
          slidesPerView: 1,
          spaceBetween: 10
        },
        // when window width is >= 768px
        768: {
          slidesPerView: 2,
          spaceBetween: 20
        },
        // when window width is >= 992px
        992: {
          slidesPerView: 3,
          spaceBetween: 20
        }
      }
    }

    safeInit(inspSlider, inspSliderConf);
  }

  // related products section slider init
  const productsSlider = document.querySelector('.products-slider');
  if( productsSlider ){
    const productsSliderConf = {
      centeredSlides: false,
      slidesPerView: 4,
      loop: true,
      navigation: {
        nextEl: productsSlider.closest('.related-products-container').querySelector('.swiper-button-next'),
        prevEl: productsSlider.closest('.related-products-container').querySelector('.swiper-button-prev'),
      },
      // Responsive breakpoints
      breakpoints: {
        // when window width is >= 320px
        320: {
          slidesPerView: 1,
          spaceBetween: 10
        },
        // when window width is >= 480px
        480: {
          slidesPerView: 1,
          spaceBetween: 10
        },
        // when window width is >= 640px
        640: {
          slidesPerView: 1,
          spaceBetween: 10
        },
        // when window width is >= 768px
        768: {
          slidesPerView: 2,
          spaceBetween: 20
        },
        // when window width is >= 992px
        992: {
          slidesPerView: 4,
          spaceBetween: 20
        }
      }
    };

    safeInit(productsSlider, productsSliderConf);
  }
  // single product gallery slider init
  // var galleryThumbs = new Swiper('.gallery-thumbs', {
  //   spaceBetween: 20,
  //   slidesPerView: 5,
  //   observer: true,
  //   observeParents: true,
  //   observeSlideChildren: true,
  //   loopedSlides: 5, //looped slides should be the same
  //   watchSlidesVisibility: true,
  //   watchSlidesProgress: true,
  // });
  // var galleryTop = new Swiper('.gallery-top', {
  //   spaceBetween: 20,
  //   observer: true,
  //   observeParents: true,
  //   observeSlideChildren: true,
  //   loopedSlides: 5, //looped slides should be the same
  //   thumbs: {
  //     swiper: galleryThumbs,
  //   },
  // });

  // sidebar listgroup widget
  const productCategories = document.querySelectorAll('.widget-list-item');

  productCategories.forEach(category => {
    let dropToggler = category.querySelector('.drop-toggler');
    let dropList = category.querySelector('.children');
    dropToggler.addEventListener('click', () => {
      category.classList.toggle("active");
    });
  });

  let showMore = document.querySelector('.short__description .show__more'),
      showLess = document.querySelector('.full__description .show__less');

  if( showMore ){
    showMore.addEventListener('click', () => {
      let wrapper = showMore.closest('.headline-description').classList.add('full');
    });
    showLess.addEventListener('click', () => {
      let wrapper = showMore.closest('.headline-description').classList.remove('full');
    });
  }

  // filter keywords
  const filtersArea = document.querySelector('.active-filters');

  keywords.forEach(filter => {
    filter.addEventListener("click", () => {
      if(filter.classList.contains('is-active')){return};
      let newFilter = document.createElement('span');
      newFilter.className='active-filters-keyword';
      newFilter.textContent = filter.textContent;
      if(filtersArea){filtersArea.appendChild(newFilter)};
      filter.classList.toggle('is-active');
      newFilter.addEventListener('click', () => {
        newFilter.remove();
        filter.classList.remove('is-active');
      })
    });
  });

  // view list toggler
  const gridBtn = document.querySelector('.grid-view');
  const listBtn = document.querySelector('.list-view');
  const columns = document.querySelectorAll('.col-md-4');
  const productCards = document.querySelectorAll('.product-card');

  if(gridBtn){
    gridBtn.addEventListener('click', () => {
      gridBtn.classList.add('active');
      listBtn.classList.remove('active');
      columns.forEach(column => column.classList.remove('col-md-12'));
      columns.forEach(column => column.classList.add('col-md-4'));
      productCards.forEach(card => card.classList.remove('horizontal'));
    })
  }

  if(listBtn){
    listBtn.addEventListener('click', () => {
      listBtn.classList.add('active');
      gridBtn.classList.remove('active');
      columns.forEach(column => column.classList.remove('col-md-4'));
      columns.forEach(column => column.classList.add('col-md-12'));
      productCards.forEach(card => card.classList.add('horizontal'));
    })
  }

  //Products categories menu
  const parentItems = document.querySelectorAll('.categories-nav li.menu-item-has-children');
  if( parentItems ){
    parentItems.forEach( (item) => {
      item.insertAdjacentHTML('afterbegin', '<span class="show-sub"></span>');
      item.querySelector('.show-sub').addEventListener('click', () =>{
        if( item.classList.contains('show') ){
          item.classList.remove('show');
        } else {
          item.classList.add('show');
        }
      });
    });
  }

  //Location modal
  const locationModal = document.querySelector('.modal-window[data-modal="map"]');
  const locationsItems = document.querySelectorAll('.modal-window[data-modal="map"] .modal-text .location');
  const locationSession = sessionStorage.getItem('showLocation');

  if( !locationSession ){
    locationModal.classList.add('show');
    document.querySelector('body').classList.add('no-scroll');
  }
  if( locationsItems ){
    locationsItems.forEach( (location) => {
      location.addEventListener('click', () => {
        let locationName = location.textContent.replace(',', '');
        sessionStorage.setItem('showLocation', locationName);
        locationModal.classList.remove('show');
        document.querySelector('body').classList.remove('no-scroll');
      });
    });
  }
});