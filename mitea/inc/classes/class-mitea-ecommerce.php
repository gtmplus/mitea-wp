<?php

class MiteaEcommerceClass {
	public function __construct(){

	}

	public function get_product_details(){
		$product__array = array(
			'title'				=> 'Bierkrug „Classic“ 0,3l',
			'article'			=> '10102',
			'packaging-unit'	=> '12',
			'description'		=> 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu.',
			'price'				=> '0,24',
			'price-vat'			=> '0,29',
			'details'			=> 'Preis pro Stück und Mieteinheit (1 Mieteinheit = 3 Tage – Sonn- und Feiertage ohne Berechnung), zzgl. Endreinigung',
			'main-image'		=> get_theme_file_uri('assets/images/product/main-image.jpg'),
			'thumbnails'		=> array(
				get_theme_file_uri('assets/images/product/product-1.jpg'),
				get_theme_file_uri('assets/images/product/product-2.jpg'),
				get_theme_file_uri('assets/images/product/product-3.jpg'),
				get_theme_file_uri('assets/images/product/product-4.jpg'),
				get_theme_file_uri('assets/images/product/product-5.jpg')
			)
		);

		return $product__array;
	}

	public function get_products(){
		$products_array = array(
			array(
				'image'			=> get_theme_file_uri('assets/images/product/product-1.jpg'),
				'name'			=> 'Weizenbierglas „Classic“ 0,5l',
				'excerpt'		=> 'Lorem ipsum dolor sit amet, onsectetuer adipiscing elit. Lorem ipsum dolor Lorem ipsum dolor sit amet, onsectetuer adipiscing elit. Lorem ipsum dolor sit amet, onsectetuer adipiscing elit. Lorem ipsum dolor Lorem ipsum dolor sit amet, onsectetuer adipiscing elit Lorem ipsum dolor …',
				'price'			=> '0,24',
				'price-vat'		=> '0,29',
				'rental-unit'	=> '12'
			),
			array(
				'image'			=> get_theme_file_uri('assets/images/product/product-2.jpg'),
				'name'			=> 'Bierkrug „Classic“ 0,3l',
				'excerpt'		=> 'Lorem ipsum dolor sit amet, onsectetuer adipiscing elit. Lorem ipsum dolor Lorem ipsum dolor sit amet, onsectetuer adipiscing elit. Lorem ipsum dolor sit amet, onsectetuer adipiscing elit. Lorem ipsum dolor Lorem ipsum dolor sit amet, onsectetuer adipiscing elit Lorem ipsum dolor …',
				'price'			=> '0,22',
				'price-vat'		=> '0,26',
				'rental-unit'	=> '15'
			),
			array(
				'image'			=> get_theme_file_uri('assets/images/product/product-3.jpg'),
				'name'			=> 'Maßkrug/Bierkrug 0,5l',
				'excerpt'		=> 'Lorem ipsum dolor sit amet, onsectetuer adipiscing elit. Lorem ipsum dolor Lorem ipsum dolor sit amet, onsectetuer adipiscing elit. Lorem ipsum dolor sit amet, onsectetuer adipiscing elit. Lorem ipsum dolor Lorem ipsum dolor sit amet, onsectetuer adipiscing elit Lorem ipsum dolor …',
				'price'			=> '0,24',
				'price-vat'		=> '0,29',
				'rental-unit'	=> '6'
			),
			array(
				'image'			=> get_theme_file_uri('assets/images/product/product-4.jpg'),
				'name'			=> 'Weizenbierglas „Classic“ 0,5l',
				'excerpt'		=> 'Lorem ipsum dolor sit amet, onsectetuer adipiscing elit. Lorem ipsum dolor Lorem ipsum dolor sit amet, onsectetuer adipiscing elit. Lorem ipsum dolor sit amet, onsectetuer adipiscing elit. Lorem ipsum dolor Lorem ipsum dolor sit amet, onsectetuer adipiscing elit Lorem ipsum dolor …',
				'price'			=> '0,20',
				'price-vat'		=> '0,21',
				'rental-unit'	=> '9'
			),
			array(
				'image'			=> get_theme_file_uri('assets/images/product/product-5.jpg'),
				'name'			=> 'Weizenbierglas „Classic“ 0,5l',
				'excerpt'		=> 'Lorem ipsum dolor sit amet, onsectetuer adipiscing elit. Lorem ipsum dolor Lorem ipsum dolor sit amet, onsectetuer adipiscing elit. Lorem ipsum dolor sit amet, onsectetuer adipiscing elit. Lorem ipsum dolor Lorem ipsum dolor sit amet, onsectetuer adipiscing elit Lorem ipsum dolor …',
				'price'			=> '0,20',
				'price-vat'		=> '0,21',
				'rental-unit'	=> '15'
			)
		);

		return $products_array;
	}

	public function get_related_products(){
		$products_array = array(
			array(
				'image'			=> get_theme_file_uri('assets/images/product/product-1.jpg'),
				'name'			=> 'Weizenbierglas „Classic“ 0,5l',
				'excerpt'		=> 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis …',
				'price'			=> '0,24',
				'price-vat'		=> '0,29',
				'rental-unit'	=> '12'
			),
			array(
				'image'			=> get_theme_file_uri('assets/images/product/product-2.jpg'),
				'name'			=> 'Bierkrug „Classic“ 0,3l',
				'excerpt'		=> 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis …',
				'price'			=> '0,22',
				'price-vat'		=> '0,26',
				'rental-unit'	=> '15'
			),
			array(
				'image'			=> get_theme_file_uri('assets/images/product/product-3.jpg'),
				'name'			=> 'Maßkrug/Bierkrug 0,5l',
				'excerpt'		=> 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis …',
				'price'			=> '0,24',
				'price-vat'		=> '0,29',
				'rental-unit'	=> '6'
			),
			array(
				'image'			=> get_theme_file_uri('assets/images/product/product-4.jpg'),
				'name'			=> 'Weizenbierglas „Classic“ 0,5l',
				'excerpt'		=> 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis …',
				'price'			=> '0,20',
				'price-vat'		=> '0,21',
				'rental-unit'	=> '9'
			),
			array(
				'image'			=> get_theme_file_uri('assets/images/product/product-5.jpg'),
				'name'			=> 'Weizenbierglas „Classic“ 0,5l',
				'excerpt'		=> 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis …',
				'price'			=> '0,20',
				'price-vat'		=> '0,21',
				'rental-unit'	=> '15'
			)
		);

		return $products_array;
	}

	public function get_cart_products(){
		$products_array = (object) array(
			'products' => array(
				array(
					'name'			=> 'Weizenbierglas „Classic“ 0,5l',
					'article'		=> '31010',
					'item-price'	=> '0,62',
					'item-amount'	=> '12',
					'price-summary'	=> '7,44',
					'extra'			=> 'Obligatorische Reinigung, ggf. Pfand:',
					'extra-text'	=> 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quod, accusamus!'
				),
				array(
					'name'			=> 'Endreinigung für 1 Teil, Silber',
					'article'		=> '85000',
					'item-price'	=> '0,26',
					'item-amount'	=> '12',
					'price-summary'	=> '3,12',
					'extra'			=> '',
					'extra-text'	=> ''
				)
			),
			'summary' => array(
				'summary'	=> '10,56',
				'vat'		=> '1,69',
				'total'		=> '12,25'
			)
		);

		return $products_array;
	}

	public function limit_text( $text, $limit ) {
	    if (str_word_count($text, 0) > $limit) {
	        $words = str_word_count($text, 2);
	        $pos   = array_keys($words);
	        $text  = substr($text, 0, $pos[$limit]) . '...';
	    }
	    return $text;
	}
}