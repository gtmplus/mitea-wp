<?php

class ThemeSettingsClass {
	const SCRIPTS_VERSION = '1.0.3';

	public function __construct(){
		$this->scriptsDir = get_theme_file_uri().'/assets/js';
        $this->stylesDir = get_theme_file_uri().'/assets/css';

		$this->actions_init();
	}

	public function actions_init(){
		add_action( 'wp_enqueue_scripts', array( $this, 'scripts_styles' ) );
		add_action( 'after_setup_theme', array( $this, 'theme_setup' ) );
		add_action( 'wp_enqueue_scripts',  array( $this, 'js_variables' ) );
		add_action( 'widgets_init', array( $this, 'widgets_init') );
		add_action( 'wp_head', array( $this, 'google_tag_manager') );
		// add_action( 'wp_head', array( $this, 'schema_org') );
		add_action( 'wp_body_open', array( $this, 'google_tag_manager_noscript') );
		add_action( 'init', array( $this, 'custom_post_type') );
		add_action( 'init', array( $this, 'custom_taxonomy') );
        
		add_filter( 'upload_mimes', array( $this, 'enable_svg_types' ), 99 );
		add_filter( 'wpseo_json_ld_output', '__return_false' );
		add_filter( 'use_block_editor_for_post', '__return_false', 10 );
		// add_filter( 'script_loader_tag', array( $this, 'add_async_attribute' ), 10, 2 );
	}

	public function scripts_styles() {
		wp_enqueue_style( 'swiper-css', 'https://unpkg.com/swiper/swiper-bundle.min.css' , '', self::SCRIPTS_VERSION);
		wp_enqueue_style( 'mitea-css', $this->stylesDir.'/styles.min.css' , '', self::SCRIPTS_VERSION);
    	wp_enqueue_style( 'mitea-style', get_stylesheet_uri() );

    	wp_enqueue_script( 'swiper-js', 'https://unpkg.com/swiper/swiper-bundle.min.js', array( 'jquery' ), self::SCRIPTS_VERSION, true );
    	wp_enqueue_script( 'all-js', $this->scriptsDir.'/all.min.js', array( 'jquery' ), self::SCRIPTS_VERSION, true );

    	if( get_field('sharethis_api_key', 'option') && is_single() ) wp_enqueue_script( 'share-js', 'https://platform-api.sharethis.com/js/sharethis.js#property='.get_field('sharethis_api_key', 'option').'&product=custom-share-buttons&cms=sop', array( 'jquery' ), self::SCRIPTS_VERSION, true );
    }

    public function theme_setup(){
    	// if( ENV == 'dev' ) update_option( 'upload_url_path', 'https://stage.bcareagency.com/wp-content/uploads', true );

    	load_theme_textdomain( 'mitea' );
	    add_theme_support( 'automatic-feed-links' );
	    add_theme_support( 'title-tag' );
	    add_theme_support( 'post-thumbnails' );

	    // add_image_size( 'exrtise-thumbnail', 270, 180, true );

	    register_nav_menus( array(
	        'main'          	=> __( 'Main Menu', 'mitea' ),
	        'footer'          	=> __( 'Footer Menu', 'mitea' ),
	       	'categories-menu'	=> __( 'Categories Menu', 'mitea' )
	    ) );

	    if( function_exists('acf_add_options_page') ) {
		    $general = acf_add_options_page(array(
		        'page_title'    => __('Theme General Settings', 'mitea'),
		        'menu_title'    => __('Theme Settings', 'mitea'),
		        'redirect'      => false,
		        'capability'    => 'edit_posts',
		        'menu_slug'     => 'theme-settings',
		    ));
		}

		add_post_type_support( 'page', 'excerpt' );
    }

    public function widgets_init(){
    	register_sidebar( array(
	        'name'          => __( 'Footer 1 (vertical menu)', 'mitea' ),
	        'id'            => 'footer-1',
	        'description'   => __( 'Add widgets here to appear in your footer.', 'mitea' ),
	        'before_widget' => '<div id="%1$s" class="widget %2$s products-categories">',
	        'after_widget'  => '</div>',
	        'before_title'  => '<h5>',
	        'after_title'   => '</h5>',
	    ) );
	    register_sidebar( array(
	        'name'          => __( 'Footer 2 (vertical menu)', 'mitea' ),
	        'id'            => 'footer-2',
	        'description'   => __( 'Add widgets here to appear in your footer.', 'mitea' ),
	        'before_widget' => '<div id="%1$s" class="widget %2$s products-categories">',
	        'after_widget'  => '</div>',
	        'before_title'  => '<h5>',
	        'after_title'   => '</h5>',
	    ) );
	    register_sidebar( array(
	        'name'          => __( 'Footer 3 (vertical menu)', 'mitea' ),
	        'id'            => 'footer-3',
	        'description'   => __( 'Add widgets here to appear in your footer.', 'mitea' ),
	        'before_widget' => '<div id="%1$s" class="widget %2$s footer-nav">',
	        'after_widget'  => '</div>',
	        'before_title'  => '<h5>',
	        'after_title'   => '</h5>',
	    ) );
    }

    public function enable_svg_types($mimes) {
		$mimes['svg'] = 'image/svg+xml';
		$mimes['svgz'] = 'image/svg+xml';
		return $mimes;
	}

	public function js_variables(){ ?>
		<script type="text/javascript">
	        var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
	    </script>
	<?php }

	public function google_tag_manager() { 
    	if ( ENV == 'prd' ) {
    		get_template_part( 'inc/analytics/gtm' );
    	}
    }

    public function schema_org() { 
    	if ( ENV == 'prd' ) {
    		get_template_part( 'inc/analytics/schema' );
    	}
    }

    public function google_tag_manager_noscript() { 
    	if ( ENV == 'prd' ) {
    		get_template_part( 'inc/analytics/gtm', 'noscript' );
    	}
    }

    public function __return_false() {
        return false;
    }

	public function add_async_attribute($tag, $handle){
	    if(!is_admin()){
	        if ('jquery-core' == $handle) {
	            return $tag;
	        }
	        return str_replace(' src', ' defer src', $tag);
	    } else {
	        return $tag;
	    }
	}

	public function custom_post_type(){
		$post_labels = array(
			'name'					=> __('Products', 'mitea'),
			'singular_name'			=> __('Product', 'mitea'),
			'add_new'				=> __('Add Product', 'mitea'),
			'add_new_item'			=> __('Add New Product', 'mitea'),
			'edit_item'				=> __('Edit Product', 'mitea'),
			'new_item'				=> __('New Product', 'mitea'),
			'view_item'				=> __('View Product', 'mitea')
		);

		$post_args = array(
			'label'               	=> __('Products', 'mitea'),
			'description'        	=> __('Product information page', 'mitea'),
			'labels'              	=> $post_labels,
			'supports'            	=> array( 'title', 'excerpt'),
			'taxonomies'          	=> array( '' ),
			'hierarchical'       	=> false,
			'public'              	=> true,
			'show_ui'             	=> true,
			'show_in_menu'        	=> true,
			'has_archive'         	=> true,
			'can_export'          	=> true,
			'show_in_nav_menus'   	=> true,
			'publicly_queryable'  	=> true,
			'exclude_from_search' 	=> false,
			'query_var'           	=> true,
			'capability_type'     	=> 'post',
			'menu_position'			=> 3,
			'rewrite'				=> array(
				'slug'				=> 'products'
			),
			'menu_icon'           	=> 'dashicons-carrot'
		);
		register_post_type( 'product', $post_args );

	}

	public function custom_taxonomy(){
		$taxonomy_labels = array(
			'name'                        => _x('Products categories', 'mitea'),
			'singular_name'               => _x('Product category', 'mitea'),
		);

		$taxonomy_rewrite = array(
			'slug'                  => 'product-category',
			'with_front'            => true,
			'hierarchical'          => true,
		);

		$taxonomy_args = array(
			'labels'              => $taxonomy_labels,
			'hierarchical'        => true,
			'public'              => true,
			'show_ui'             => true,
			'show_admin_column'   => true,
			'show_in_nav_menus'   => true,
			'show_tagcloud'       => true,
			'rewrite'             => $taxonomy_rewrite,
		);
		register_taxonomy( 'product-category', 'product', $taxonomy_args );

		$taxonomy_labels = array(
			'name'                        => _x('Products tags', 'mitea'),
			'singular_name'               => _x('Product tag', 'mitea'),
		);

		$taxonomy_rewrite = array(
			'slug'                  => 'product-tag',
			'with_front'            => true,
			'hierarchical'          => false,
		);

		$taxonomy_args = array(
			'labels'              => $taxonomy_labels,
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_admin_column'   => true,
			'show_in_nav_menus'   => true,
			'show_tagcloud'       => true,
			'rewrite'             => $taxonomy_rewrite,
		);
		register_taxonomy( 'product-tag', 'product', $taxonomy_args );
	}
}

$theme_settings = new ThemeSettingsClass();