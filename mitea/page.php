<?php
/**
 *
 * @package WordPress
 * @subpackage Mitea
 * @since 1.0
 * @version 1.0
 */

get_header(); 

get_template_part( 'template-parts/page/content', 'banner' );
get_template_part( 'template-parts/page/content', 'breadcrumbs' );
if( have_rows('content') ):

	while ( have_rows('content') ) : the_row();

		if( get_row_layout() == 'cart_table' ):
			get_template_part( 'template-parts/page/content', 'cart' );
		elseif( get_row_layout() == 'content_editor' ):
			get_template_part( 'template-parts/page/content', 'editor' );
		elseif( get_row_layout() == 'hero_section' ):
			get_template_part( 'template-parts/page/content', 'hero_section' );
		elseif( get_row_layout() == 'posts_section' ):
			get_template_part( 'template-parts/page/content', 'posts_section' );
		elseif( get_row_layout() == 'image_section' ):
			get_template_part( 'template-parts/page/content', 'image_section' );
		elseif( get_row_layout() == 'products_slider_section' ):
			get_template_part( 'template-parts/page/content', 'products_slider_section' );
		elseif( get_row_layout() == 'text_section' ):
			get_template_part( 'template-parts/page/content', 'text_section' );
		
		endif;
	endwhile;

endif;

get_footer();