<section class="section section-products">
  <div class="container">
    <?php if( get_sub_field('title') ) { ?><h2 class="section-title"><?php the_sub_field('title'); ?></h2><?php } ?>
    <?php 
    $categories = get_sub_field('choose_categories');

    if ( $categories ) : ?>
    <div class="row">
      <?php foreach ( $categories as $category ) { 
        $thumbnail = get_field('thumbnail', 'product-category_'.$category) ? ' style="background-image: url('.get_field('thumbnail', 'product-category_'.$category)['url'].')"' : ''; 
        $term = get_term( $category, 'product-category'); 
        ?>
      <div class="col-md-3 col-sm-6">
        <div class="category-card small">
          <div class="image-wrapper">
            <div class="category-image"<?php echo $thumbnail; ?>></div>
          </div>
          <p class="category-title text-md-left"><?php echo $term->name; ?></p>
          <a href="<?php echo get_term_link($category, 'product-category'); ?>" class="stretched-link"></a>
        </div>
      </div>
      <?php } ?>
    </div>
  <?php endif; ?>
  </div>
</section>