<section class="section section-recommended">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="section-content">
          <!--HEADLINE-->
          <?php if( get_sub_field('title') ) { ?>
          <div class="section-headline">
            <h2 class="headline-title"><?php the_sub_field('title'); ?></h2>
          </div>
          <?php } ?>
        </div>
      </div>
    </div>
    <?php if( get_sub_field('choose_product') ) { 
      $args = array(
        'post_type'     => 'product',
        'post_in'       => get_sub_field('choose_product'),
        'orderby'       => 'post__in',
        'posts_per_page'=> 4
      ); 
      $query = new WP_Query($args);
      $i = 1;
      if ( $query->have_posts() ) : ?>
      <div class="row align-items-xl-center">
        <?php while ( $query->have_posts() ) : $query->the_post(); 
          $ecommerce = new MiteaEcommerceClass();
          $numb_array = array_rand(array(0,1,2,3,4));
          $product = $ecommerce->get_products()[$numb_array];
          ?>
        <?php if( $i == 1 ) { ?>
          <div class="col-md-6 col-xl-3 mt-sm-2">
            <div class="feature-card">
            <div class="card-image">
              <div class="thumbnail" style="background-image: url(<?php echo $product['image']; ?>);"></div>
            </div>
            <div class="card-text-content">
              <p class="card-title"><?php echo $product['name']; ?></p>
              <p class="card-description"><?php echo $ecommerce->limit_text($product['excerpt'], 22); ?></p>
              <div class="card-price">
                <div class="card-price-netto"><span class="price"><?php echo $product['price']; ?> €</span><?php _e('zzgl. MwSt.', 'mitea'); ?></div>
                <div class="card-price-brutto"><span class="price"><?php echo $product['price-vat']; ?> €</span><?php _e('inkl. MwSt.', 'mitea'); ?></div>
              </div>
              <a href="<?php the_permalink(); ?>" class="btn btn-primary"><?php _e('Paket ansehen', 'mitea'); ?></a>
            </div>
          </div>
        </div>
        <?php } elseif( $i == 2 ) { ?>
          <div class="col-md-6 col-xl-3 mt-4 mt-sm-2">
            <div class="feature-card">
            <div class="card-image">
              <div class="thumbnail" style="background-image: url(<?php echo $product['image']; ?>);"></div>
            </div>
            <div class="card-text-content">
              <p class="card-title"><?php echo $product['name']; ?></p>
              <p class="card-description"><?php echo $ecommerce->limit_text($product['excerpt'], 22); ?></p>
              <div class="card-price">
                <div class="card-price-netto"><span class="price"><?php echo $product['price']; ?> €</span><?php _e('zzgl. MwSt.', 'mitea'); ?></div>
                <div class="card-price-brutto"><span class="price"><?php echo $product['price-vat']; ?> €</span><?php _e('inkl. MwSt.', 'mitea'); ?></div>
              </div>
              <a href="<?php the_permalink(); ?>" class="btn btn-primary"><?php _e('Paket ansehen', 'mitea'); ?></a>
            </div>
          </div>
        </div>
        <?php } elseif( $i == 3 ) { ?>
          <div class="col-lg-12 col-xl-6">
            <div class="row">
              <div class="col-12">
                <div class="feature-card horizontal">
                  <div class="card-text-content">
                    <p class="card-title"><?php echo $product['name']; ?></p>
                    <p class="card-description"><?php echo $ecommerce->limit_text($product['excerpt'], 17); ?></p>
                    <div class="card-price">
                      <div class="card-price-netto"><span class="price"><?php echo $product['price']; ?> €</span><?php _e('zzgl. MwSt.', 'mitea'); ?></div>
                      <div class="card-price-brutto"><span class="price"><?php echo $product['price-vat']; ?> €</span><?php _e('inkl. MwSt.', 'mitea'); ?></div>
                    </div>
                    <a href="<?php the_permalink(); ?>" class="btn btn-primary"><?php _e('Paket ansehen', 'mitea'); ?></a>
                  </div>
                  <div class="card-image">
                    <div class="thumbnail" style="background-image: url(<?php echo $product['image']; ?>);"></div>
                  </div>
                </div>
              </div>
            </div>
        <?php } elseif( $i == 4 ) { ?>
            <div class="row">
              <div class="col-12">
                <div class="feature-card horizontal">
                  <div class="card-text-content">
                    <p class="card-title"><?php echo $product['name']; ?></p>
                    <p class="card-description"><?php echo $ecommerce->limit_text($product['excerpt'], 17); ?></p>
                    <div class="card-price">
                      <div class="card-price-netto"><span class="price"><?php echo $product['price']; ?> €</span><?php _e('zzgl. MwSt.', 'mitea'); ?></div>
                      <div class="card-price-brutto"><span class="price"><?php echo $product['price-vat']; ?> €</span><?php _e('inkl. MwSt.', 'mitea'); ?></div>
                    </div>
                    <a href="<?php the_permalink(); ?>" class="btn btn-primary"><?php _e('Paket ansehen', 'mitea'); ?></a>
                  </div>
                  <div class="card-image">
                    <div class="thumbnail" style="background-image: url(<?php echo $product['image']; ?>);"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        <?php } 
        $i++; endwhile; ?>
      </div>
      <?php wp_reset_postdata(); endif; 
    } ?>
  </div>
</section>