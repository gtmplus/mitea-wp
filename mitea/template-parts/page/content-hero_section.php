<section class="section section-intro">
  <!--SECTION-SLIDER-->
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-5">
        <?php 
        $images = get_sub_field('images');
        if( $images ) { ?>
        <!-- Swiper Slider-->
        <div class="intro-slider">
          <div class="swiper-wrapper">
            <?php foreach ( $images as $image ) { ?>
            <div class="swiper-slide">
              <div class="image" style="background-image: url(<?php echo $image['url']; ?>);"></div>
            </div>
            <?php } ?>
          </div>
          <!-- Add Arrows -->
          <div class="swiper-buttons">
            <div class="swiper-button swiper-button-prev"></div>
            <div class="swiper-button swiper-button-next"></div>
            <!-- Pagination -->
            <div class="swiper-dots"></div>
          </div>
        </div>
        <?php } ?>
      </div>
      <!--SECTION-CONTENT-->
      <div class="col-md-5">
        <div class="section-content">
          <!--HEADLINE-->
          <?php if( get_sub_field('title') ) { ?>
          <div class="section-headline">
            <h2 class="headline-title"><?php the_sub_field('title'); ?></h2>
          </div>
          <?php } ?>
          <!--DESCRIPTION-->
          <?php if( get_sub_field('text') ) { ?>
          <div class="headline-description"><?php the_sub_field('text'); ?></div>
          <?php } ?>
          <!--SEARCH-->
          <div class="section-search">
            <form role="search" method="get" class="mta-search__form search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
              <div class="mta-search__group">
                <input type="search" class="mta-search__field searchbar" value="<?php echo get_search_query(); ?>" name="s" placeholder="<?php _e('Was suchen Sie?', 'mitea'); ?>" />
                <button type="submit" class="mta-search__submit search-icon"></button>
              </div>
            </form>
            <?php 
            $args = array(
              'taxonomy'    => 'product-tag',
              'hide_empty'  => false
            );
            $terms = get_terms($args); 
            if( $terms ) { ?>
            <div class="search-keywords">
              <?php foreach ( $terms as $term ) { ?>
              <a href="<?php echo get_term_link($term->term_id, 'product-tag'); ?>" class="btn btn-primary search-keyword"><?php echo $term->name; ?></a>
              <?php } ?>
            </div>
            <?php } ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>