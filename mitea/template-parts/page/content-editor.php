<div class="container">
    <article class="post">
      <div class="headline-content">
        <h2 class="headline-title"><?php the_title(); ?></h2>
      </div>
      <div class="post-content">
        <?php the_sub_field('text'); ?>
      </div>
    </article>
</div>