<?php 
$background = get_field('page', 'option')['banner_image'] ? ' style="background-image: url('.get_field('page', 'option')['banner_image']['url'].');"' : '';
?>
<div class="header-search"<?php echo $background; ?>>
	<?php get_search_form(); ?>
</div>