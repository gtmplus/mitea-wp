<?php 
$ecommerce = new MiteaEcommerceClass();
$cart = $ecommerce->get_cart_products();
?>
<div class="container">
  <section class="section-cart">
    <?php if( get_sub_field('title') ) { ?>
    <h2 class="headline-title"><?php the_sub_field('title'); ?></h2>
    <?php } ?>
    <div class="row">
      <div class="col-md-12">
        <div class="table-responsive">
          <table class="table">
            <thead>
              <tr>
                <th scope="col" class="text-left pl-0"><?php _e('Pos.', 'mitea'); ?></th>
                <th scope="col"><?php _e('Art-Nr.', 'mitea'); ?></th>
                <th scope="col"><?php _e('Artikelbezeichnung', 'mitea'); ?></th>
                <th scope="col" class="text-right"><?php _e('Einzelpreis (zzgl. MwSt.)', 'mitea'); ?></th>
                <th scope="col" class="text-center"><?php _e('Anzahl', 'mitea'); ?></th>
                <th scope="col" class="text-center"><?php _e('Aktion', 'mitea'); ?></th>
                <th scope="col" class="text-right pr-0"><?php _e('Preis (zzgl. MwSt.)', 'mitea'); ?></th>
              </tr>
            </thead>
            <tbody>
              <?php if( $cart ) { 
                $i = 1;
                foreach ( $cart->products as $product ) { ?>
                <tr>
                  <th scope="row" class="text-left"><?php echo $i; ?></th>
                  <td><?php echo $product['article']; ?></td>
                  <td><a href="#" class="product-link link"><?php echo $product['name']; ?></a></td>
                  <td><div class="product-price text-right"><?php echo $product['item-price']; ?> EUR</div></td>
                  <td><input type="number" class="product-counter text-center" value="<?php echo $product['item-amount']; ?>"></input></td>
                  <td class="text-center">
                    <button type="button" class="product-remove-btn">
                      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                      <defs/>
                      <path d="M424 64h-88V48c0-26.467-21.533-48-48-48h-64c-26.467 0-48 21.533-48 48v16H88c-22.056 0-40 17.944-40 40v56c0 8.836 7.164 16 16 16h8.744l13.823 290.283C87.788 491.919 108.848 512 134.512 512h242.976c25.665 0 46.725-20.081 47.945-45.717L439.256 176H448c8.836 0 16-7.164 16-16v-56c0-22.056-17.944-40-40-40zM208 48c0-8.822 7.178-16 16-16h64c8.822 0 16 7.178 16 16v16h-96zM80 104c0-4.411 3.589-8 8-8h336c4.411 0 8 3.589 8 8v40H80zm313.469 360.761A15.98 15.98 0 01377.488 480H134.512a15.98 15.98 0 01-15.981-15.239L104.78 176h302.44z"/>
                      <path d="M256 448c8.836 0 16-7.164 16-16V224c0-8.836-7.164-16-16-16s-16 7.164-16 16v208c0 8.836 7.163 16 16 16zM336 448c8.836 0 16-7.164 16-16V224c0-8.836-7.164-16-16-16s-16 7.164-16 16v208c0 8.836 7.163 16 16 16zM176 448c8.836 0 16-7.164 16-16V224c0-8.836-7.164-16-16-16s-16 7.164-16 16v208c0 8.836 7.163 16 16 16z"/>
                      </svg>
                    </button>
                  </td>
                  <td class="pr-0"><div class="product-total-price text-right"><?php echo $product['price-summary']; ?> EUR</div></td>
                </tr>
                <?php if( $product['extra'] ) { ?>
                <tr>
                  <th scope="row"></th>
                  <td></td>
                  <td><div class="discount-info">
                    <?php echo $product['extra']; ?>
                  </div></td>
                  <td colspan="4">
                    <div class="discount-text">
                      <?php echo $product['extra-text']; ?>
                    </div>
                  </td>
                </tr>
                <?php } 
                $i++; }
              } ?>
            </tbody>
        </table>
      </div>
      
      <div class="cart-summury">
        <div class="row align-items-end justify-content-between">
          <div class="col-lg-7">
            <?php if( get_sub_field('text') ) { ?>
            <div class="cart-info">
              <?php the_sub_field('text'); ?>
            </div>
            <?php } ?>
          </div>
          <?php if( $cart->summary ) { ?>
          <div class="col-lg-4">
            <div class="order-pricing">
              <div class="col-md-12">
                <div class="order-price">
                  <div class="row justify-content-between">
                    <div class="col-8 text-left"><?php _e('Gesamt', 'mitea'); ?></div>
                    <div class="col-4 text-right"><?php echo $cart->summary['summary']; ?> EUR</div>
                  </div>
                </div>
              </div>
              <div class="col-md-12">
                <div class="order-tax">
                  <div class="row justify-content-between my-4">
                    <div class="col-8 text-left"><?php _e('zzgl 16% Mwst', 'mitea'); ?></div>
                    <div class="col-4 text-right"><?php echo $cart->summary['vat']; ?> EUR</div>
                  </div>
                </div>
              </div>
              <div class="col-md-12">
                <div class="order-total-price">
                  <div class="row justify-content-between">
                    <div class="col-8 text-left"><?php _e('Gesamtpreis inkl. Mwst:', 'mitea'); ?></div>
                    <div class="col-4 text-right"><?php echo $cart->summary['total']; ?> EUR</div>
                  </div>
                </div>
              </div>
              <div class="col-md-12">
                <div class="cart-buttons">
                  <a href="#" class="order-btn btn request-btn"><?php _e('Anfrage stellen', 'mitea'); ?></a>
                  <a href="#" class="order-btn btn offer-btn"><?php _e('Bestellung senden', 'mitea'); ?></a>
                </div>
              </div>
            </div>
          </div>
          <?php } ?>
        </div>
      </div>
    </div>
  </section>
</div>