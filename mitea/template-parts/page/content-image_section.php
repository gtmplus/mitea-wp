<?php if( get_sub_field('image') ) { ?>
<div class="image-separator">
	<img src="<?php echo get_sub_field('image')['url']; ?>" alt="<?php echo get_sub_field('image')['title']; ?>">
</div>
<?php } ?>