<section class="section section-inspiration">
  <div class="container">
    <?php if( get_sub_field('title') ) { ?>
    <h2 class="section-title"><?php the_sub_field('title'); ?></h2>
    <?php } 
    if( get_sub_field('choose_category_to_show') ) { 
      $args = array(
        'post_type'     => 'post',
        'cat'           => get_sub_field('choose_category_to_show'),
        'posts_per_page'=> get_sub_field('posts_count')
      );
      $query = new WP_Query($args);
      if ( $query->have_posts() ) : ?>
      <!-- Swiper -->
      <div class="slider-wrapper">
        <div class="inspiration-slider">
          <div class="swiper-wrapper">
            <?php while ( $query->have_posts() ) : $query->the_post(); 
              $thumbnail = get_the_post_thumbnail_url( get_the_ID() ) ? ' style="background-image: url('.get_the_post_thumbnail_url( get_the_ID(), 'full').')"' : '';
              ?>
            <div class="swiper-slide">
              <div class="insp-post">
                <div class="thumbnail"<?php echo $thumbnail; ?>></div>
                <a href="<?php the_permalink(); ?>" class="insp-post-title stretched-link"><?php the_title(); ?></a>
              </div>
            </div>
            <?php endwhile; ?>
          </div>
        </div>
        <div class="swiper-button swiper-button-prev"></div>
        <div class="swiper-button swiper-button-next"></div>
      </div>
      <?php wp_reset_postdata(); endif;
    } ?>
  </div>
</section>