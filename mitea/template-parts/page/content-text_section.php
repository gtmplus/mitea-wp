<?php
$background = get_sub_field('background') ? ' style="background-image: url('.get_sub_field('background').')"' : '';
?>
<section class="section section-rent"<?php echo $background; ?>>
  <div class="container">
    <div class="row align-items-md-center">
      <div class="col-md-6">
        <div class="section-content">
          <?php if( get_sub_field('title') ) { ?>
          <div class="section-headline">
            <h2 class="headline-title"><?php the_sub_field('title'); ?></h2>
          </div>
          <?php } ?>
          <?php if( get_sub_field('text') ) { ?>
          <div class="headline-description">
            <?php the_sub_field('text'); ?>
          </div>
          <?php } 
          if( get_sub_field('button_url') ) { ?>
          <a href="<?php the_sub_field('button_url'); ?>" class="btn btn-primary"><?php the_sub_field('button_label'); ?></a>
          <?php } ?>
        </div>
      </div>
    </div>
  </div>
</section>