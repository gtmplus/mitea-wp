<section class="section section-features">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="section-content">
          <!--HEADLINE-->
          <?php if( get_sub_field('title') ) { ?>
          <div class="section-headline">
            <h2 class="headline-title"><?php the_sub_field('title'); ?></h2>
          </div>
          <?php } ?>
          <!--DESCRIPTION-->
          <?php if( get_sub_field('title') ) { ?>
          <div class="headline-description">
            <?php the_sub_field('text'); ?>
          </div>
          <?php } ?>

          <?php if( get_sub_field('slider_title') ) { ?>
            <h3 class="section-title"><?php the_sub_field('slider_title'); ?></h3>
          <?php } 
          $categories = get_sub_field('choose_tags_to_show'); 
          if( $categories ) { ?>
          <div class="features-container">
            <ul class="nav nav-pills" id="pills-tab">
              <?php 
              $i = 0;
              foreach ( $categories as $category ) {
                $category_detail = get_term($category, 'product-tag');
                $class = $i == 0 ? ' active' : '';
              ?>
              <li class="nav-item">
                <div class="nav-link<?php echo $class; ?>" data-tag="<?php echo $category_detail->slug; ?>"><?php echo $category_detail->name; ?></div>
              </li>
              <?php $i++; } ?>
            </ul>
            <div class="tab-content" id="pills-tabContent">
              <?php 
              $i = 0;
              foreach ( $categories as $category ) { 
                $category_detail = get_term($category, 'product-tag');
                $class = $i == 0 ? ' show active' : '';
                $args = array(
                  'post_type'     => 'product',
                  'posts_per_page'=> -1,
                  'tax_query' => array(
                      array(
                        'taxonomy' => 'product-tag',
                        'field'    => 'term_id',
                        'terms'    => $category
                      )
                    )
                );
                $query = new WP_Query($args);
              ?>
              <div class="tab-pane fade<?php echo $class; ?>" data-tag="<?php echo $category_detail->slug; ?>">
                <!-- Swiper -->
                <?php if ( $query->have_posts() ) : ?>
                <div class="tab-slider tab-slider-<?php echo $category_detail->slug; ?>">
                  <div class="swiper-wrapper">
                    <?php while ( $query->have_posts() ) : $query->the_post(); 
                      $ecommerce = new MiteaEcommerceClass();
                      $numb_array = array_rand(array(0,1,2,3,4));
                      $product = $ecommerce->get_products()[$numb_array];
                      ?>
                    <div class="swiper-slide">
                      <div class="feature-card">
                        <div class="card-image">
                          <div class="thumbnail" style="background-image: url(<?php echo $product['image']; ?>);"></div>
                        </div>
                        <div class="card-text-content">
                          <p class="card-title"><?php echo $product['name']; ?></p>
                          <p class="card-description">
                            <?php echo $ecommerce->limit_text($product['excerpt'], 17); ?>
                          </p>
                          <div class="card-price">
                            <div class="card-price-netto"><span class="price"><?php echo $product['price']; ?> €</span>zzgl. MwSt.</div>
                            <div class="card-price-brutto"><span class="price"><?php echo $product['price-vat']; ?> €</span>inkl. MwSt.</div>
                          </div>
                          <a href="<?php the_permalink(); ?>" class="btn btn-primary"><?php _e('Paket ansehen', 'mitea'); ?></a>
                        </div>
                      </div>
                    </div>
                    <?php endwhile; ?>
                  </div>
                </div>
                <div class="swiper-button swiper-button-next"></div>
                <div class="swiper-button swiper-button-prev"></div>
                <?php wp_reset_postdata(); endif; ?>
              </div>
              <?php $i++; } ?>
            </div>
          </div>
          <?php } ?>
        </div>
      </div>
    </div>
  </div>
</section>