<?php 
$ecommerce = new MiteaEcommerceClass();
$numb_array = array_rand(array(0,1,2,3,4));
$product = $ecommerce->get_related_products()[$numb_array];
?>
<div class="product-card">
  <a href="<?php the_permalink(); ?>" class="product-card-img" style="background-image: url(<?php echo $product['image']; ?>);"></a>
    <a href="<?php the_permalink(); ?>" class="product-card-title stretched-link"><?php echo $product['name']; ?></a>
    <div class="card-price">
      <div class="card-price-netto"><span class="price"><?php echo $product['price']; ?> €</span>zzgl. MwSt.</div>
      <div class="card-price-brutto"><span class="price"><?php echo $product['price-vat']; ?> €</span>inkl. MwSt.</div>
  </div>
</div>