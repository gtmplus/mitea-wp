<?php 
$ecommerce = new MiteaEcommerceClass();
$numb_array = array_rand(array(0,1,2,3,4));
$product = $ecommerce->get_related_products()[$numb_array];
?>
<div class="col-md-4" data-id="<?php the_ID(); ?>">
  <div class="product-card">
    <div class="product-img-overlay">
      <div class="overlay-content-inner">
        <a href="<?php the_permalink(); ?>" class="overlay-content-item details-link"><?php _e('Produktdetails', 'mitea'); ?></a>
        <div class="overlay-content-item product-meta">
          <span class="product-meta-quantity"><?php _e('Stückzahl', 'mitea'); ?></span>
          <input class="product-meta-counter" type="number" value="<?php echo $product['rental-unit']; ?>">
          <a href="#" class="product-meta-cart">
            <!-- <img src="img/shopping-cart.svg" alt="To Cart"> -->
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -31 512 512"><path d="M164.96 300.004h.024c.02 0 .04-.004.059-.004H437a15.003 15.003 0 0014.422-10.879l60-210a15.003 15.003 0 00-2.445-13.152A15.006 15.006 0 00497 60H130.367l-10.722-48.254A15.003 15.003 0 00105 0H15C6.715 0 0 6.715 0 15s6.715 15 15 15h77.969c1.898 8.55 51.312 230.918 54.156 243.71C131.184 280.64 120 296.536 120 315c0 24.812 20.188 45 45 45h272c8.285 0 15-6.715 15-15s-6.715-15-15-15H165c-8.27 0-15-6.73-15-15 0-8.258 6.707-14.977 14.96-14.996zM477.114 90l-51.43 180H177.032l-40-180zM150 405c0 24.813 20.188 45 45 45s45-20.188 45-45-20.188-45-45-45-45 20.188-45 45zm45-15c8.27 0 15 6.73 15 15s-6.73 15-15 15-15-6.73-15-15 6.73-15 15-15zm167 15c0 24.813 20.188 45 45 45s45-20.188 45-45-20.188-45-45-45-45 20.188-45 45zm45-15c8.27 0 15 6.73 15 15s-6.73 15-15 15-15-6.73-15-15 6.73-15 15-15zm0 0"/></svg>
          </a>
        </div>
      </div>
      <a href="<?php the_permalink(); ?>" class="product-card-img" style="background-image: url(<?php echo $product['image']; ?>);"></a>
    </div>
    <div class="product-card-info">
      <a href="<?php the_permalink(); ?>" class="product-card-title"><?php echo $product['name']; ?></a>
      <div class="card-price">
        <div class="card-price-netto"><span class="price"><?php echo $product['price']; ?> €</span><?php _e('zzgl. MwSt.', 'mitea'); ?></div>
        <div class="card-price-brutto"><span class="price"><?php echo $product['price-vat']; ?> €</span><?php _e('inkl. MwSt.', 'mitea'); ?></div>
      </div>
    </div>
  </div>
</div>