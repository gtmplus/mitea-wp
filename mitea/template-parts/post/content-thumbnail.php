<?php 
$thumbnail = get_the_post_thumbnail_url( get_the_ID() ) ? ' style="background-image: url('.get_the_post_thumbnail_url( get_the_ID(), 'full').')"' : '';
?>
<div class="col-md-4">
  <div class="category-card">
      <div class="image-wrapper">
        <div class="category-image"<?php echo $thumbnail; ?>></div>
      </div>
      <p class="category-title text-md-left"><?php the_title(); ?></p>
      <a href="<?php the_permalink(); ?>" class="stretched-link"></a>
    </div>
</div>