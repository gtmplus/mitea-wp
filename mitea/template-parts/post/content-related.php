<?php 
$recommended = get_field('recommended_products');
?>
<div class="container">
	<section class="related-products">
      <?php 
      $title = $recommended['title'] ? $recommended['title'] : __('Ähnliche Produkte', 'mitea');
      if( $title ) { ?>
        <h2 class="section-title"><?php echo $title; ?></h2>
      <?php } 
      $products = $recommended['choose_product_to_show']; 
      if( $products ) { 
        $args = array(
          'post_type'       => 'product',
          'posts_per_page'  => -1,
          'orderby'         => 'post__in',
          'post__in'        => $products,
          'post__not_in'    => [get_the_ID()]
        );
      } else {
        if( is_singular( 'product' ) ){
          $terms = get_the_terms(get_the_ID(), 'product-category');
          $cats = [];
          foreach ( $terms as $term ) {
            $cats[] = $term->term_id;
          }
          $args = array(
            'post_type'       => 'product',
            'posts_per_page'  => -1,
            'orderby'         => 'post__in',
            'post__not_in'    => [get_the_ID()],
            'tax_query'       => array(
              array(
                'taxonomy'    => 'product-category',
                'field'       => 'term_id',
                'terms'       => $cats
              )
            )
          );
          
        } else {
          $args = array(
            'post_type'       => 'product',
            'posts_per_page'  => -1,
            'orderby'         => 'rand'
          );
        }
      }
        $query = new WP_Query($args);

        if ( $query->have_posts() ) : ?>
        <!-- Swiper -->
        <div class="related-products-container">
          <div class="products-slider">
            <div class="swiper-wrapper">
              <?php while ( $query->have_posts() ) : $query->the_post(); ?>
              <div class="swiper-slide">
                <?php get_template_part( 'template-parts/product/content', 'thumbnail' ); ?>
              </div>
              <?php endwhile; ?>
            </div>
          </div>
          <!-- Add Arrows -->
          <div class="swiper-related-buttons">
            <div class="swiper-button swiper-button-prev"></div>
            <div class="swiper-button swiper-button-next"></div>
          </div>
        </div>
        <?php wp_reset_postdata(); 
        endif; ?>
    </section>
</div>